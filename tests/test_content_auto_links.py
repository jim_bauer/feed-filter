#
# Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# This software is licensed according to the included LICENSE file
# SPDX-License-Identifier: GPL-3.0-or-later
#

import pytest
from feedfilter.feed_filter import content_auto_links


@pytest.mark.parametrize("data,expected", [
    (
        '<p>Blah: https://www.example.com/1 </p>',
        '<p>Blah: <a href="https://www.example.com/1">https://www.example.com/1</a> </p>'
    ),
    (
        '<p>lala https://www.example.com/2</p>',
        '<p>lala <a href="https://www.example.com/2">https://www.example.com/2</a></p>'
    ),
    (
        '<p>lala https //www.example.com/3</p>',
        '<p>lala https //www.example.com/3</p>'
    ),
    (
        '<p>https://www.example.com/4',
        '<p><a href="https://www.example.com/4">https://www.example.com/4</a>'
    ),
    (
        '<p><a href="https://www.example.com/5">https://www.example.com/5</a>',
        '<p><a href="https://www.example.com/5">https://www.example.com/5</a>'
    ),
])
def test_add_links(data, expected):
    actual = content_auto_links(data)
    assert expected == actual
