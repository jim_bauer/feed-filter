#
# Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# This software is licensed according to the included LICENSE file
# SPDX-License-Identifier: GPL-3.0-or-later
#

import pytest

import datetime
from feedfilter.feed_filter import MangleTitle


@pytest.fixture
def dt():
    return datetime.datetime.fromisoformat('2022-12-01 01:02:03+00:00')


def test_title_nochange(dt):
    mt = MangleTitle()
    title_in = 'Re: nice title'
    expected = title_in
    actual = mt.mangle(title_in, dt)
    assert actual == expected


def test_title_add_date(dt):
    mt = MangleTitle(add_date=True)
    title_in = 'Re: nice title'
    expected = title_in # well, it starts with this
    actual = mt.mangle(title_in, dt)
    assert str(dt) in actual
    assert actual.startswith(expected)


def test_title_no_recolon(dt):
    mt = MangleTitle(title_re=r'([^•]+ • )?(Re: )?(\[[^]]+] )?(.*)',
                     title_sub=r'\1\3\4')
    title_in = 'Re: nice title'
    expected = 'nice title'
    actual = mt.mangle(title_in, dt)
    assert actual == expected


def test_title_extract_simple(dt):
    mt = MangleTitle(title_re=r'([^•]+ • )?(Re: )?(\[[^]]+] )?(.*)',
                     title_sub=r'\4')
    title_in = 'Forum name • Re: [solved] nice title'
    expected = 'nice title'
    actual = mt.mangle(title_in, dt)
    assert actual == expected
