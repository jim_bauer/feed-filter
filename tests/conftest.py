#
# Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# This software is licensed according to the included LICENSE file
# SPDX-License-Identifier: GPL-3.0-or-later
#

import pytest

import feedfilter.logger as logger


@pytest.fixture(autouse=True)
def loginit():
    logger.init(None, level='error')
