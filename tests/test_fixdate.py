#
# Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# This software is licensed according to the included LICENSE file
# SPDX-License-Identifier: GPL-3.0-or-later
#

from feedfilter.feed_filter import fix_date


def test_fixdate_without_tz():
    orig = '2022-12-12T21:32:25'
    expected = '2022-12-12 21:32:25+00:00'
    actual = str(fix_date(orig))
    assert actual == expected, 'fix_date wrong'


def test_fixdate_with_tz():
    orig = '2022-12-12T21:32:25-05:00'
    expected = '2022-12-12 21:32:25-05:00'
    actual = str(fix_date(orig))
    assert actual == expected, 'fix_date wrong'
