#
# Copyright (c) 2022 Jim Bauer <4985656-jim_bauer@users.noreply.gitlab.com>
# This software is licensed according to the included LICENSE file
# SPDX-License-Identifier: GPL-3.0-or-later
#

CLEAN_DIRS 	:= . $(shell find * -type d)
CLEAN_PATTERNS	= *~ .*~ __pycache__ *.egg-info

builds		:= $(wildcard dist/*.whl)


.PHONY: default
default:


.PHONY: install-requirements
install-requirements:
	pip install poetry
	poetry install


.PHONY: dist build
dist build:
	poetry build


.PHONY: tests test
tests test:
	pytest tests/


.PHONY: pipx-install
pipx-install:
	@newest=$(firstword $(builds));				\
	for f in $(builds); do					\
		test "$$f" -nt "$$newest" && newest="$$f";	\
	done;							\
	if test -f "$$newest"; then				\
		echo pipx install --force "$$newest";		\
		pipx install --force "$$newest";		\
	else							\
		echo "No builds found";				\
		exit 1;						\
	fi


.PHONE: pipx-uninstall
pipx-uninstall:
	pipx uninstall feedfilter



.PHONY: clean
clean:
	@for d in $(CLEAN_DIRS); do			\
		if test -d $$d; then			\
			echo "Cleaning in $$d";		\
			cd $$d;				\
			$(RM) -r $(CLEAN_PATTERNS);	\
			cd $(CURDIR);			\
		fi					\
	done;

.PHONY: distclean
distclean: clean
	$(RM) -r dist
